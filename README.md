CookieEnabler
=============

jquery cookie plugin for EU Cookie Law

Import this 2 files in your website.

You need <a href='http://jquery.com/download/'>jquery</a> and <a href='http://plugins.jquery.com/cookie/'>jquery.cookie</a>

How to use it:
=============

    //init
    var cookieEnabler = new CookieEnabler({
        cookieText: "This page........ cookies... <br /> Select which one cookies..... (your own text)",
        levels: ["Google analitik", "Tracking", "Favourites", "Your own levels"],
        cookieName: "customCookieNameIfYouWant"
    });

    //change settings event
    $("#yourSelector").click(function () {
        cookieEnabler.changeSettings(); 
    });
    
This is how it looks like:

![alt tag](http://shrani.si/f/3K/zP/2B85lxEK/lookslike.png)
    
Checking if enabled
=============

You can simple check if your cookie if enabled:

    //checking if your level is enabled
    if (cookieEnabler.isEnabled("Tracking")) {
        //u can track
    }

or disabled:

    //checking if your level is disabled
    if (cookieEnabler.isDisabled("Tracking")) {
        //u can NOT track
    }
    
