﻿/*Kainoto jquery cookie plugin for EU Cookie Law
  jquery.cookie plugin needed
  Author: Klemen Žarn
*/
function CookieEnabler(options) {
    this.enabledCookies = {};
    this.firstTime = true;
    this.settings = $.extend({
        cookieText: "Stran uporablja piškotke za shranjevanje priljubljenih vsebin in za google analitik, blabla... <br /> Izberite si katere piškotke na spletni strani dovolite: ",
        cookieName: "cookieEnabler",
        levels: ["Google analitik", "Priljubljeno"] //do not place commas here, like "google, analitics, etc
    }, options);

    for (var i = 0; i < this.settings.levels.length; i++) {
        this.enabledCookies[this.settings.levels[i]] = false;
    }

    this.start();
};

CookieEnabler.prototype.start = function () {
    var cookie = $.cookie(this.settings.cookieName);
    if (cookie != null) {
        //exist
        cookie = cookie.split(",");
        for (var i = 0; i < cookie.length; i++) {
            this.enabledCookies[cookie[i]] = true;
        }
    } else {
        //cookie not yet created, asking user for interaction
        for (var i = 0; i < this.settings.levels.length; i++) {
            this.enabledCookies[this.settings.levels[i]] = true;
        }
        $.cookie(this.settings.cookieName, this.settings.levels, { expires: 365, path: '/' });
        this.createDialog();
    }
};

CookieEnabler.prototype.createDialog = function () {
    this.firstTime = false;
    var cookieName = this.settings.cookieName;
    var dialog = $("<div class='cookeEnabler' />");
    var checkBoxes = $("<div class='cookieChoser' />");
    var temp = "";
    for (var i = 0; i < this.settings.levels.length; i++) {
        var isChecked = this.isEnabled(this.settings.levels[i]) == true ? "checked" : "";
        temp += "<input type='checkbox' class='cookieEnabledCheckBox' " + isChecked + " id='" + this.settings.levels[i] + "' value='" + this.settings.levels[i] + "'/><label for='" + this.settings.levels[i] + "'>" + this.settings.levels[i] + "</label><br />";
    }
    checkBoxes.html(temp);
    var buttonAgree = $("<button id='cookieEnablerButton' />");
    dialog.html(this.settings.cookieText).append(checkBoxes).append(buttonAgree);
    $(document.body).append(dialog);

    $("#cookieEnablerButton").click(function () {
        var enabledCookies = new Array();
        $(".cookieEnabledCheckBox").each(function () {
            if ($(this).is(':checked'))
                enabledCookies.push($(this).val());
        });
        dialog.remove();
        $.cookie(cookieName, enabledCookies, { expires: 365, path: '/' });
        location.reload();
    });
}

CookieEnabler.prototype.isEnabled = function (levelName) {
    if (this.enabledCookies[levelName] != undefined) {
        return this.enabledCookies[levelName];
    }
    return false;
}

CookieEnabler.prototype.isDisabled = function (levelName) {
    if (this.enabledCookies[levelName] != undefined) {
        return !this.enabledCookies[levelName];
    }
    return true;
}


CookieEnabler.prototype.changeSettings = function () {
    if (this.firstTime)
        this.createDialog();
}